### README ###

The idea of this app is to make community of people that use local sports centers. The app offers a platform through users exchange experiences with the sports centers or explore new centers in their area. Users can search the right sport center for them via the search engine and then view details about sport center (sport activities included, location, working hours, address), leave feedback and rating. The app offers two modes of interaction: anonymous and signed-in, and a module for user registration, so everybody can join the community and start leaving feedback and ratings.

The app also has a back-end part, where authorized administrator can manage centers (add or remove sport activities, edit details, add geolocation, manage comments), fix reports (registered users have an option to report a problem if they find one, so administrator have an interface where he can see the reports and easily access center and user management part of the app) and manage users (block them if they behave inappropriately).


The back-end part of the application is written in PHP/MySQL (can be found [here](https://bitbucket.org/nodinodi/scf_backend)) and the app is communicating with it through custom made API, using JSON.