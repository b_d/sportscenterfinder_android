package com.example.scfmobile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SearchListDisplayAdapter extends ArrayAdapter<Item> {


    WindowManager wm;
    SessionManager manager;
    private List<Item> items;
    GlobalValues global;
    Context currentContext;

    public SearchListDisplayAdapter(Context context, int resource, List<Item> items, WindowManager _wm) {

        super(context, resource, items);

        this.items = items;
        this.wm = _wm;
        this.manager = new SessionManager(context);
        this.global = (GlobalValues) context.getApplicationContext();
        this.currentContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {

            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.search_list_display_item, null);

        }

        Item p = items.get(position);

        if (p != null) {

            TextView name = (TextView) v.findViewById(R.id.center_name);
            TextView street = (TextView) v.findViewById(R.id.street);
            TextView county = (TextView) v.findViewById(R.id.county);
            TextView country = (TextView) v.findViewById(R.id.country);
            TextView description = (TextView) v.findViewById(R.id.description);
            TextView rating = (TextView) v.findViewById(R.id.rating);
            ImageView map = (ImageView) v.findViewById(R.id.map);
            TextView description_text = (TextView) v.findViewById(R.id.description_text);
            Button btnComments = (Button)v.findViewById(R.id.btnShowComments);


            if (name != null) {
                name.setText(" " + p.getName());
                Log.d("TAG", p.getName());
            }
            if (street != null) {
                street.setText(" " + p.getStreet() + ", " + p.getPost() + ", " + p.getTown());
                Log.d("TAG", p.getStreet());
            }
            if (county != null) {
                if (p.getCounty().equals("")) {
                    county.setVisibility(View.INVISIBLE);
                    county.getLayoutParams().height = 0;
                }
                county.setText(" " + p.getCounty());
            }
            if (country != null) {
                if (p.getCounty().equals("")) country.setVisibility(View.INVISIBLE);
                country.setText(" " + p.getCountry());
            }
            if (description != null) {

                if (p.getDescription().equals("")) {
                    description.setVisibility(View.INVISIBLE);
                    description.getLayoutParams().height = 0;
                    description_text.setVisibility(View.INVISIBLE);
                    description_text.getLayoutParams().height = 0;
                }
                description.setText(" " + p.getDescription());
            }
            if (rating != null) {

                rating.setText(" " + p.getRating());
            }
            if (map != null) {
                new RetriveImage(map).execute(p.getUrl());
            }
           
            if (btnComments != null && manager.isLoggedIn()) {

                final String str = new String(p.getId());
                btnComments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new CommentsGetter().execute(str);                       
                    }
                });
            }
        }

        return v;

    }

    /**
     * \brief RetriveImage gets image for given url and sets ImageView
     */
    class RetriveImage extends AsyncTask<String, Void, Bitmap> {

        ImageView map;

        public RetriveImage(ImageView _map) {
            this.map = _map;
        }

        protected Bitmap doInBackground(String... urls) {

            Bitmap bitmap = null;
            InputStream is = null;
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                httpClient.getParams().setParameter("http.protocol.content-charset", "UTF-8");
                HttpGet httpGet = new HttpGet(urls[0].replace(" ", "%20"));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                bitmap = BitmapFactory.decodeStream(is);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NetworkOnMainThreadException e) {
                Log.e("NETWORK ERROR", "Image" + e.toString());
            } catch (NullPointerException e) {
                Log.e("NETWORK ERROR", "Image" + e.toString());
            } catch (RuntimeException e) {
                Log.e("NETWORK ERROR", "Image" + e.toString());
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap _bitmap) {
            // TODO: check this.exception
            // TODO: do something with the feed
            map.setImageBitmap(_bitmap);
        }
    }

    class CommentsGetter extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id",strings[0]));
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"generateComments.php", "GET", params);
            try {
                if(jsonObject.getInt("status")==100){
                    //ok
                    //Log.d("JSON", jsonObject.toString());
                    String json = jsonObject.toString();

                    Intent i = new Intent();
                    i.setClass(currentContext, Comments.class);
                    i.putExtra("extra",jsonObject.toString());
                    currentContext.startActivity(i);

                }else if(jsonObject.getInt("status")== 101){
                    //zero comments

                }else if(jsonObject.getInt("status") == 102){
                    //error

                }
            }catch (JSONException e){
                Log.e("JSON", e.toString());
            }

            return null;
        }
    }
}
