package com.example.scfmobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ReportCenter extends Activity {

    String centerId, jsonString;
    Button btnReport, btnReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_center);
        getIntentData();
        btnReturn = (Button)findViewById(R.id.button);
        btnReport = (Button)findViewById(R.id.button1);

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    private void getIntentData() {
        Intent intent;
        intent = getIntent();
        jsonString = intent.getStringExtra("json");
        Log.d("dd","dd");
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
                centerId = jsonObject.getString("center_id");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onDestroy();
        Intent i = new Intent();
        i.setClass(getApplication(), Comments.class);
        i.putExtra("extra",jsonString);
        startActivity(i);
    }
}
