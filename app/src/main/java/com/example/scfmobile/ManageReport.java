package com.example.scfmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ManageReport extends Activity {

    GlobalValues global;
    SessionManager manager;
    public String center_id, center, report_id, status, email, username;
    Button btnManageCenter, btnManageUser;

    public TextView tvUsername, tvTime, tvCenter, tvDescription, tvSuggestion;
    public CheckBox cbStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_report);
        manager = new SessionManager(getApplicationContext());
        global = (GlobalValues) getApplicationContext();

        tvTime = (TextView) findViewById(R.id.tvTime);
        tvCenter = (TextView) findViewById(R.id.tvCenter);
        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvSuggestion = (TextView) findViewById(R.id.tvSuggestion);

        cbStatus = (CheckBox) findViewById(R.id.cbStatus);

        btnManageCenter = (Button) findViewById(R.id.btnManageCenter);
        btnManageUser = (Button) findViewById(R.id.btnManageUser);

        getIntentData();
    }

    public void getIntentData(){
        Intent intent;
        intent = getIntent();

        center_id = intent.getStringExtra("center_id");
        center = intent.getStringExtra("center_name");
        report_id = intent.getStringExtra("error_id");
        email = intent.getStringExtra("email");
        username = intent.getStringExtra("username");

        tvCenter.setText(center);
        tvTime.setText(intent.getStringExtra("time"));
        tvUsername.setText(username);
        tvDescription.setText(intent.getStringExtra("problem"));
        tvSuggestion.setText(intent.getStringExtra("suggestion"));


        btnManageCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent();
                        i.setClass(getApplicationContext(), ManageCenters.class);
                        i.putExtra("center",center);
                        i.putExtra("center_id",center_id);
                        startActivity(i);
                    }
                });

            }
        });

        btnManageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent();
                        i.setClass(getApplicationContext(), ManageUser.class);
                        i.putExtra("username",username);
                        startActivity(i);
                    }
                });

            }
        });

        if (intent.getStringExtra("status").equals("1")){
            cbStatus.setChecked(true);
        }
        cbStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(cbStatus.isChecked()){
                    status = "1";
                }else{
                    status = "0";
                }
                new UpdateReport().execute();
            }
        });
    }
    public void ManageUser(View v) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent();
                i.setClass(getApplicationContext(), ManageUser.class);
                i.putExtra("username",username);
                startActivity(i);
            }
        });
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, Reports.class));
    }

    private class UpdateReport extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("report_id",report_id));
            params.add(new BasicNameValuePair("status",status));

            JSONParser jsonparser = new JSONParser();
            JSONObject jsonObject = jsonparser.makeHttpRequest(global.getHost() + "updateReport.php", "GET", params);
            Log.d("JSON", jsonObject.toString());
            try {
                Boolean queryStatus = jsonObject.getBoolean("query");

                if (!queryStatus) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Update failed!", Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if(status.equals("1")){
                                new SendEmail().execute();
                            }
                            Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

    }
    private class SendEmail extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("error_id",report_id));
            params.add(new BasicNameValuePair("email",email));
            params.add(new BasicNameValuePair("username",username));

            JSONParser jsonparser = new JSONParser();
            jsonparser.makeHttpRequest(global.getHost() + "emailUser.php", "POST", params);


            return null;
        }

    }
}