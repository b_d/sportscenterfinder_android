package com.example.scfmobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchUsers extends Activity {
    //Globals
    GlobalValues global;
    SessionManager manager;
    String username;
    List<String> usernames;
    private ProgressDialog pDialog;
    private AutoCompleteTextView actv;
    String[] polje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_users);
        global = (GlobalValues) getApplicationContext();
        manager = new SessionManager(getApplicationContext());
        actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);

        usernames = new ArrayList<String>();
        pDialog = new ProgressDialog(SearchUsers.this);
        pDialog.show();

        new getUsers().execute();

    }
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        actv.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.unlogged_search, menu);
        return true;
    }

    public void setField(){
        polje = new String[ usernames.size() ];
        usernames.toArray( polje );

        final ArrayAdapter adapter = new ArrayAdapter (this,android.R.layout.simple_list_item_1,polje);
        actv.setAdapter(adapter);
        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(getApplicationContext(), etName.getText().toString(), Toast.LENGTH_LONG).show();

                        Intent i = new Intent();
                        i.setClass(getApplicationContext(), ManageUser.class);
                        i.putExtra("username",adapter.getItem(position).toString());
                        startActivity(i);
                    }
                });

            }
        });
    }

    class getUsers extends AsyncTask<String, String, String> {


        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject;

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            setField();
        }
        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            jsonObject = jsonParser.makeHttpRequest(global.getHost()+"getUsers.php", "GET", params);

            try {
                JSONArray users = jsonObject.getJSONArray("users");
                for (int i=0; i<users.length();i++) {
                    JSONObject currentUser = users.getJSONObject(i);
                    usernames.add(currentUser.getString("username"));
                    usernames.add(currentUser.getString("email"));
                }
                }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }



    }

}
