package com.example.scfmobile;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewCenter extends Activity {

    private static String url = "http://10.0.2.2/scf2DBManage/getActivites.php";
    public ArrayAdapter<String> listAdapter;
    GlobalValues global;
    SessionManager manager;
    //Globals
    String TAG = "Unllogged search TAG";
    String ERROR = "Unllogged search ERROR";
    //Search params
    String name, town, county, street, country, availableActivity, sort, post;
    EditText etName, etStreet, etTown, etCounty, etCountry, etPost;
    RadioGroup rgSort;
    Spinner sp;
    List<String> ids;
    List<String> activities;
    List<String> checkedActivities;
    List<CheckBox> checkBoxList;
    ListView lvActivities;
    ActivitiesAdapter lvActivitiesAdapter;
    //JSON TAGs
    String STATUS_TAG = "status";
    String CENTERS_TAG = "centers";
    String NAME_TAG = "name";
    String ID_TAG = "center_id";
    String LAT_TAG = "lat";
    String LNG_TAG = "lng";
    String STREET_TAG = "street";
    String TOWN_TAG = "town";
    String COUNTY_TAG = "county";
    String COUNTRY_TAG = "country";
    String ACTIVITIES_TAG = "activities";
    String ACTIVITY_ID_TAG = "activity_id";
    String ACTIVITY_TAG = "activity";
    String RATING_TAG = "rating";
    String POST_TAG = "post";
    String DESCRIPTION_TAG = "description";
    String ADDED_BY_USER_TAG = "added_by_user";
    String MAP_URL_TAG = "url";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        global = (GlobalValues) getApplicationContext();
        manager = new SessionManager(getApplicationContext());
        checkBoxList = new ArrayList<CheckBox>();
        new ActivitiesGetter().execute();
        etInit();


    }

    private void etInit() {

        etName = (EditText) findViewById(R.id.input_name);
        etStreet = (EditText) findViewById(R.id.input_street);
        etPost = (EditText) findViewById(R.id.input_post);
        etTown = (EditText) findViewById(R.id.input_town);
        etCounty = (EditText) findViewById(R.id.input_county);
        etCountry = (EditText) findViewById(R.id.input_country);
        lvActivities = (ListView) findViewById(R.id.input_activites);
        checkedActivities = new ArrayList<String>();
        activities = new ArrayList<String>();
        activities.clear();
        ids = new ArrayList<String>();
        ids.clear();
        lvActivitiesAdapter = new ActivitiesAdapter(getApplicationContext(), R.layout.spinner_item_radio, activities, checkedActivities, checkBoxList);
        lvActivities.setAdapter(lvActivitiesAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.unlogged_search, menu);
        return true;
    }

    public void save(View clickedButton) {
        //default value
        name = street = town = county = country = post = "";
        //center info
        name = etName.getText().toString();
        street = etStreet.getText().toString();
        post = etPost.getText().toString();
        town = etTown.getText().toString();
        county = etCounty.getText().toString();
        country = etCountry.getText().toString();

        Log.d("SELECTED IDS", String.valueOf(checkedActivities.size()));
        if (checkedActivities.size() == 0 || name.equals("") || street.equals("") /*&& post.isEmpty() */ || town.equals("") /* && county.isEmpty() */ || country.equals("")) {
            Toast.makeText(getApplicationContext(), "Fill all required fileds!", Toast.LENGTH_LONG).show();
        } else {
            new SaveCenter().execute();
        }

    }

    private void clearAllETs() {
        etName.setText("");
        etStreet.setText("");
        etPost.setText("");
        etTown.setText("");
        etCounty.setText("");
        etCountry.setText("");
        if (checkBoxList.size() == 0) {
            for (int i = 0; i < checkBoxList.size(); i++) {
                checkBoxList.get(i).setChecked(false);
            }
        }
    }

    class SaveCenter extends AsyncTask<String, String, String> {

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String activity;
            params.add(new BasicNameValuePair("username", manager.getUsername()));
            params.add(new BasicNameValuePair("name", name));
            params.add(new BasicNameValuePair("street", street));
            params.add(new BasicNameValuePair("town", town));
            params.add(new BasicNameValuePair("county", county));
            params.add(new BasicNameValuePair("country", country));
            int i = 0;
            for (String value : checkedActivities) {
                params.add(new BasicNameValuePair("activites[" + i + "]", value));
            }
            params.add(new BasicNameValuePair("post", post));

            jsonObject = jsonParser.makeHttpRequest(global.getHost()+"addNewCenter.php", "POST", params);

            Log.d("JSON RESPONSE TAG", jsonObject.toString());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (jsonObject.getBoolean("center") == false) {
                            Log.d("JSON RESPONSE TAG", "upis centra");
                            Toast.makeText(getApplicationContext(), "Center insert failed!", Toast.LENGTH_LONG).show();
                        } else if (jsonObject.getBoolean("geocode") == false) {
                            Log.d("JSON RESPONSE TAG", "Center address failed to geocode!");
                            Toast.makeText(getApplicationContext(), "Center address failed to geocode!", Toast.LENGTH_LONG).show();
                        } else if (jsonObject.getBoolean("query") == false) {
                            Toast.makeText(getApplicationContext(), "Center insert failed!", Toast.LENGTH_LONG).show();
                        } else if (jsonObject.getBoolean("activities") == false) {
                            Toast.makeText(getApplicationContext(), "Center insert failed!", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_LONG).show();
                            clearAllETs();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
        }
    }

    /**
     * \brief ActivitiesGetter gets all activity info from DB
     */

    private class ActivitiesGetter extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {

            lvActivitiesAdapter.notifyDataSetChanged();
            lvActivities.forceLayout();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"getActivites.php", "GET", null);
            Log.d("---------------Response----------", jsonObject.toString());
            try {
                if (jsonObject.getInt(STATUS_TAG) == 100) {
                    JSONArray ja = jsonObject.getJSONArray(ACTIVITIES_TAG);
                    activities.clear();

                    ids.clear();

                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jObj = ja.getJSONObject(i);
                        ids.add(jObj.getString(ACTIVITY_ID_TAG));
                        activities.add(jObj.getString(ACTIVITY_TAG));
                        Log.i("ID:", jObj.getString(ACTIVITY_ID_TAG));
                        Log.i("ACTIVITY:", jObj.getString(ACTIVITY_TAG));
                    }
                }
            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }
            return null;
        }
    }

    class ActivitiesAdapter extends ArrayAdapter<String> {

        List<String> items;
        List<String> selected;
        List<CheckBox> boxList;

        public ActivitiesAdapter(Context context, int textViewResourceId, List<String> _items, List<String> _selected, List<CheckBox> _boxList) {
            super(context, textViewResourceId, _items);
            this.items = _items;
            this.selected = _selected;
            this.boxList = _boxList;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final int pos = position;
            View row = convertView;


            if (row == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                row = vi.inflate(R.layout.spinner_item_radio, null);
                lvActivities.setLayoutParams(new TableLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, 1f));

            }
            String p = items.get(position);

            if (p != null) {
                CheckBox chbox = (CheckBox) row.findViewById(R.id.item);

                boxList.add(chbox);
                chbox.setText(p);
                chbox.setChecked(false);

                chbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b == true) {
                            //button checked
                            if (!selected.contains(ids.get(pos))) {
                                selected.add(ids.get(pos));
                            }
                        } else {
                            //button unchecked
                            if (selected.contains(ids.get(pos))) {
                                int r = selected.indexOf(ids.get(pos));
                                selected.remove(r);
                            }
                        }
                        Log.d("Count of", String.valueOf(selected.size()));
                    }
                });
            }
            return row;
        }
    }
}