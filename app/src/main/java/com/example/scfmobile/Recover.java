package com.example.scfmobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.scfmobile.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Recover extends Activity {

    GlobalValues global;
    Button btnEmail, btnAktivacija, btnPromjena;
    EditText etEmail, etKey, etPass, etRepeat, etOldPass;
    private ProgressDialog pDialog;
    public String email = "", pass, repeatPass, oldPass;
    LinearLayout llEmail, llAktivacija, llPromjena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover);
        global = (GlobalValues) getApplicationContext();

        llEmail = (LinearLayout) findViewById(R.id.unos_emaila);
        llAktivacija = (LinearLayout) findViewById(R.id.unos_aktivacije);
        llPromjena = (LinearLayout) findViewById(R.id.promjena_passworda);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etKey = (EditText) findViewById(R.id.etKey);
        etPass = (EditText)findViewById(R.id.etNewPass);
        etRepeat = (EditText)findViewById(R.id.etRepeatPass);


        btnEmail = (Button) findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                email = etEmail.getText().toString();
                if(isEmailValid(email)){
                    pDialog = new ProgressDialog(Recover.this);
                    pDialog.show();
                    new checkEmail().execute();
                }else {
                    Toast.makeText(getApplicationContext(), "Enter a valid email address!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnAktivacija = (Button) findViewById(R.id.btnAktivacija);
        btnAktivacija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new ProgressDialog(Recover.this);
                pDialog.show();
                new activate().execute();

            }
        });
        btnPromjena = (Button) findViewById(R.id.btnPromjena);
        btnPromjena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pass = etPass.getText().toString();
                repeatPass = etRepeat.getText().toString();

                if (!pass.equals("") && !repeatPass.equals("") ) {
                    if (pass.length() >= 6) {
                        if (pass.equals(repeatPass)) {
                            pDialog = new ProgressDialog(Recover.this);
                            pDialog.show();
                            new change().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), "Passwords don't match!", Toast.LENGTH_LONG).show();
                            etPass.setText("");
                            etRepeat.setText("");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Password length must be over 6 characters!", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Enter data to all fields!!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public static boolean isEmailValid(String str) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = str;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public void return_msg(Integer s){
        if(s == 1){
            Toast.makeText(Recover.this, "Provjerite mail i unesite aktivacijski kod!", Toast.LENGTH_SHORT).show();
            llAktivacija.setVisibility(View.VISIBLE);
            llEmail.setVisibility(View.GONE);
        }else if (s == -1){
            Toast.makeText(Recover.this, "Unjeli ste mail koji ne postoji u bazi!!", Toast.LENGTH_SHORT).show();
        }
    }

    public void check_key(Integer s){
        if(s == 1){
            //Toast.makeText(Recover.this, "Nadjeeen", Toast.LENGTH_SHORT).show();
            llPromjena.setVisibility(View.VISIBLE);
            llAktivacija.setVisibility(View.GONE);
        }else if (s == -1){
            Toast.makeText(Recover.this, "Unjeli ste neispravni kljuc!", Toast.LENGTH_SHORT).show();
        }
    }

    public void check_change(Integer s){
        if(s == 1){
            Toast.makeText(Recover.this, "Promjenjen password, ulogirajte se!", Toast.LENGTH_SHORT).show();
            finish();
        }else if (s == -1){
            Toast.makeText(Recover.this, "Unjeli ste neispravnu staru lozinku!!", Toast.LENGTH_SHORT).show();
        }
    }

    private class checkEmail extends AsyncTask<Void, Void, Void> {
        private Integer status;

        @Override
        protected void onPostExecute(Void aVoid) {

            pDialog.dismiss();
            return_msg(status);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("email", email));
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"recoverPassword.php", "POST", param);
            Log.d("Response", jsonObject.toString());
            try {
                Boolean nadjen = jsonObject.getBoolean("nadjen");
                if (nadjen == true) {
                    Boolean mail = jsonObject.getBoolean("mail");
                    if (mail == true)
                        status = 1;
                }else{
                    status = -1;
                }
            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }

            return null;
        }
    }
    private class activate extends AsyncTask<Void, Void, Void> {
        private Integer status;

        @Override
        protected void onPostExecute(Void aVoid) {

            pDialog.dismiss();
            check_key(status);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("email", email));
            param.add(new BasicNameValuePair("key", etKey.getText().toString()));
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"checkRecovery.php", "POST", param);
            Log.d("Response", jsonObject.toString());
            try {
                Boolean nadjen = jsonObject.getBoolean("nadjen");
                if (nadjen == true) {
                    status = 1;
                }else{
                    status = -1;
                }
            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }

            return null;
        }
    }
    private class change extends AsyncTask<Void, Void, Void> {
        private Integer status;

        @Override
        protected void onPostExecute(Void aVoid) {

            pDialog.dismiss();
            check_change(status);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("newPass", pass ));
            param.add(new BasicNameValuePair("email", email ));
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"changePassword.php", "POST", param);
            Log.d("Response", jsonObject.toString());
            try {
                Boolean oldPass = jsonObject.getBoolean("oldPass");
                Boolean query = jsonObject.getBoolean("query");
                if (oldPass == true) {
                    if(query == true){
                        status = 1;
                    }
                }else{
                    status = -1;
                }
            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }

            return null;
        }
    }
}
