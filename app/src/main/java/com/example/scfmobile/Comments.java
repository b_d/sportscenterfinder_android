package com.example.scfmobile;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Comments extends Activity {
    List<List<String>> commentList;
    ListView lvComments;
    Button btnSave,btnReport;
    EditText etNewComment;
    String centerId;
    GlobalValues global;
    SessionManager manager;
    CommentsListAdapter adapter;
    Intent intent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        commentList = new ArrayList<List<String>>();
        getIntentData();

        manager = new SessionManager(getApplicationContext());
        global = (GlobalValues) getApplicationContext();
        lvComments = (ListView) findViewById(R.id.comment_list);
        adapter = new CommentsListAdapter(getApplicationContext(), R.layout.comment_item,commentList);
        lvComments.setAdapter(adapter);
        etNewComment = (EditText) findViewById(R.id.new_comment);
        btnSave = (Button) findViewById(R.id.save_comment);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CommentAdder().execute(etNewComment.getText().toString());

            }
        });
        btnReport = (Button)findViewById(R.id.report_center);
        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(getApplication(), Comments.class);
                i.putExtra("json",intent.getStringExtra("extra"));
                startActivity(i);
            }
        });

    }
    private void getIntentData() {

        intent = getIntent();
        String jsonString = intent.getStringExtra("extra");
//        new CommentsGetter().execute(intent.getStringExtra("extra"));
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            if (jsonObject.getInt("status") == 100) {
                centerId = jsonObject.getString("center_id");
                JSONArray centers = jsonObject.getJSONArray("comments");
                commentList.clear();
                for (int i = 0; i < centers.length(); i++) {
                    List<String> commentAttrs = new ArrayList<String>();
                    JSONObject comment = centers.getJSONObject(i);

                    commentAttrs.clear();

                    commentAttrs.add(comment.optString("comment_id"));
                    commentAttrs.add(comment.optString("comment"));
                    commentAttrs.add(comment.optString("by_user"));
                    commentAttrs.add(comment.optString("time"));
                    commentList.add(commentAttrs);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class CommentsListAdapter extends ArrayAdapter<List<String>>{
        List<List<String>> comments;
        SessionManager manager;
        public CommentsListAdapter(Context context, int textViewResourceId, List<List<String>> objects) {
            super(context, textViewResourceId, objects);
            this.comments = objects;
            manager = new SessionManager(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.comment_item, null);
            }

            TextView tvHeader = (TextView) v.findViewById(R.id.comment_header);
            TextView tvBody = (TextView) v.findViewById(R.id.comment_body);
            Button btn = (Button) v.findViewById(R.id.delete_comment);

            List<String> comment = comments.get(position);

            if (tvHeader != null){
                tvHeader.setText("Added by: \""+ comment.get(2)+"\" at "+comment.get(3));

            }
            if (tvBody!= null){
                tvBody.setText("Comment: "+comment.get(1) );
            }
            if (btn!=null){
                if (comment.get(2).equals(manager.getUsername())){
                    final String tmpCommentId = new String(comment.get(0));
                    final String tmpComment = new String(comment.get(1));
                    final String tmpCommentTime = new String(comment.get(3));
                    final String pos = String.valueOf(position);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new CommentDeleter().execute(tmpCommentId, tmpComment, tmpCommentTime, pos);
                        }
                    });
                }else{
                    btn.getLayoutParams().height = 0;
                    btn.setVisibility(View.INVISIBLE);
                }
            }


            return v;
        }


    }

    class CommentAdder extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("comment",strings[0]));
            params.add(new BasicNameValuePair("username",manager.getUsername()));
            params.add(new BasicNameValuePair("id",centerId));
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"addComment.php", "GET", params);
            try {
                if(jsonObject.getBoolean("query")){
                    //ok
                    final String str = new String(strings[0]);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),"Comment added!", Toast.LENGTH_LONG).show();
                            etNewComment.setText("");
                            List<String> tmp = new ArrayList<String>();
                            tmp.clear();

                            tmp.add("");
                            tmp.add(str);
                            tmp.add(manager.getUsername());
                            tmp.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                            commentList.add(tmp);
                        }
                    });
                }else {
                    //zero comments

                }
            }catch (JSONException e){
                Log.e("JSON", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            adapter.notifyDataSetChanged();

        }
    }

    class CommentDeleter extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("comment_id",strings[0]));
            params.add(new BasicNameValuePair("comment",strings[1]));
            params.add(new BasicNameValuePair("time",strings[2]));
            params.add(new BasicNameValuePair("user", manager.getUsername()));
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"deleteComment.php", "GET", params);
            Log.d("JSON", jsonObject.toString());
            try {
                if(jsonObject.getBoolean("query")){
                    //ok
                    final String position = new String(strings[3]);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),"Comment deleted!", Toast.LENGTH_LONG).show();

                            finish();
                        }
                    });
                }else {
                    //zero comments

                }
            }catch (JSONException e){
                Log.e("JSON", e.toString());
            }

            return null;
        }
    }
}