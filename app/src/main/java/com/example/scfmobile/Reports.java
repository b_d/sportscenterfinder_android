package com.example.scfmobile;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Reports extends Activity {

    GlobalValues global;
    WindowManager wm;
    private ProgressDialog pDialog;
    List<Report> reportList1, reportList2;
    ReportListDisplayAdapter adapter, adapter2;
    ListView lv, lv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        global = (GlobalValues) getApplicationContext();

        lv = (ListView) findViewById(R.id.report_list);
        lv2 = (ListView) findViewById(R.id.report_list2);
        reportList1 = new ArrayList<Report>();
        reportList2 = new ArrayList<Report>();
        wm = getWindowManager();
        pDialog = new ProgressDialog(Reports.this);
        pDialog.show();
        new getReports().execute();

    }


    private void setAdapters(){
        adapter = new ReportListDisplayAdapter(this, R.layout.reports_display_item, reportList1, wm);
        lv.setAdapter(adapter);
        adapter2 = new ReportListDisplayAdapter(this, R.layout.reports_display_item, reportList2, wm);
        lv2.setAdapter(adapter2);

        lv.setOnItemClickListener(myClickListener);
        lv2.setOnItemClickListener(myClickListener);
    }

    private AdapterView.OnItemClickListener myClickListener = new AdapterView.OnItemClickListener(){

        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
            final TextView tvSuggestion = (TextView) v.findViewById(R.id.tvSuggestion);
            final TextView tvCenter = (TextView) v.findViewById(R.id.tvCenter);
            final TextView tvTime = (TextView) v.findViewById(R.id.tvTime);
            final TextView tvErrorId = (TextView) v.findViewById(R.id.tvErrorId);
            final TextView tvUsername = (TextView) v.findViewById(R.id.tvUsername);
            final TextView tvCenterId = (TextView) v.findViewById(R.id.tvCenterId);
            final TextView tvProblem = (TextView) v.findViewById(R.id.tvProblem);
            final TextView tvStatus = (TextView) v.findViewById(R.id.tvStatus);
            final TextView tvEmail = (TextView) v.findViewById(R.id.tvEmail);

           // Toast.makeText(getApplication(), tvSuggestion.getText().toString(), Toast.LENGTH_SHORT).show();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent();
                    i.setClass(getApplicationContext(), ManageReport.class);
                    i.putExtra("suggestion", tvSuggestion.getText().toString());
                    i.putExtra("center_name", tvCenter.getText().toString());
                    i.putExtra("time", tvTime.getText().toString());
                    i.putExtra("error_id", tvErrorId.getText().toString());
                    i.putExtra("username", tvUsername.getText().toString());
                    i.putExtra("center_id", tvCenterId.getText().toString());
                    i.putExtra("problem", tvProblem.getText().toString());
                    i.putExtra("status",tvStatus.getText().toString());
                    i.putExtra("email",tvEmail.getText().toString());
                    startActivity(i);
                }
            });
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reports, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private class getReports extends AsyncTask<Void, Void, Void> {
        String county, country, street, town, post_num, lat, lng, desc, rating;

        @Override
        protected void onPostExecute(Void aVoid) {
            pDialog.dismiss();
            setAdapters();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("status", "0"));

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"getReports.php", "GET", param);
            Log.d("Response", jsonObject.toString());
            try {

                JSONArray reports = jsonObject.getJSONArray("reports");
                for (int i=0; i<reports.length();i++) {

                    JSONObject currentReport = reports.getJSONObject(i);

                    List<NameValuePair> currentReportInfo = new ArrayList<NameValuePair>();
                    currentReportInfo.clear();
                    currentReportInfo.add(new BasicNameValuePair("report_id", currentReport.getString("report_id")));
                    currentReportInfo.add(new BasicNameValuePair("id_center",currentReport.getString("id_center")));
                    currentReportInfo.add(new BasicNameValuePair("problem",currentReport.getString("problem")));
                    currentReportInfo.add(new BasicNameValuePair("time",currentReport.getString("time")));
                    currentReportInfo.add(new BasicNameValuePair("suggestion",currentReport.getString("suggestion")));
                    currentReportInfo.add(new BasicNameValuePair("user",currentReport.getString("user")));
                    currentReportInfo.add(new BasicNameValuePair("seen",currentReport.getString("seen")));
                    currentReportInfo.add(new BasicNameValuePair("center_name",currentReport.getString("center_name")));
                    currentReportInfo.add(new BasicNameValuePair("email",currentReport.getString("email")));
                    currentReportInfo.add(new BasicNameValuePair("status","0"));
                    reportList1.add(new Report(currentReportInfo));

                }

            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }

            param.clear();
            param.add(new BasicNameValuePair("status", "1"));

            jsonParser = new JSONParser();
            jsonObject = jsonParser.makeHttpRequest(global.getHost()+"getReports.php", "GET", param);
            Log.d("Response", jsonObject.toString());
            try {

                JSONArray reports = jsonObject.getJSONArray("reports");
                for (int i=0; i<reports.length();i++) {

                    JSONObject currentReport2 = reports.getJSONObject(i);

                    List<NameValuePair> currentReportInfo2 = new ArrayList<NameValuePair>();
                    currentReportInfo2.clear();
                    currentReportInfo2.add(new BasicNameValuePair("report_id", currentReport2.getString("report_id")));
                    currentReportInfo2.add(new BasicNameValuePair("id_center",currentReport2.getString("id_center")));
                    currentReportInfo2.add(new BasicNameValuePair("problem",currentReport2.getString("problem")));
                    currentReportInfo2.add(new BasicNameValuePair("time",currentReport2.getString("time")));
                    currentReportInfo2.add(new BasicNameValuePair("suggestion",currentReport2.getString("suggestion")));
                    currentReportInfo2.add(new BasicNameValuePair("user",currentReport2.getString("user")));
                    currentReportInfo2.add(new BasicNameValuePair("seen",currentReport2.getString("seen")));
                    currentReportInfo2.add(new BasicNameValuePair("center_name",currentReport2.getString("center_name")));
                    currentReportInfo2.add(new BasicNameValuePair("email",currentReport2.getString("email")));
                    currentReportInfo2.add(new BasicNameValuePair("status","1"));
                    reportList2.add(new Report(currentReportInfo2));
                }

            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }

            return null;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, AdminLogged.class));
    }
}
