package com.example.scfmobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Registration will provide user registration methods
 */
public class Registration extends Activity {

    private EditText etUsername, etName, etLastname, etEmail, etPhone, etMobile, etPass, etRepeatPass;// etTown, etStreet, etPost, etState;

    private String username, email, mobile, pass, phone, repeatPass, name, lastname;// town, street, post, state;
    private ProgressDialog pDialog;

    GlobalValues global;

    private static String registerUrl = "register.php";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        global = (GlobalValues) getApplicationContext();

        etUsername = (EditText) findViewById(R.id.username);
        etName = (EditText) findViewById(R.id.newUserName);
        etLastname = (EditText) findViewById(R.id.newUserLastname);
        etEmail = (EditText) findViewById(R.id.email);
        etPhone = (EditText) findViewById(R.id.phone_num);
        etMobile = (EditText) findViewById(R.id.mobile_num);
        etPass = (EditText) findViewById(R.id.pass);
        etRepeatPass = (EditText) findViewById(R.id.repeat_pass);
    }

    /**
     * This method will get values from user and send it to async task for completing registration
     *
     * @param clickedButton
     */
    public void register(View clickedButton) {
        //user info
        username = etUsername.getText().toString();
        pass = etPass.getText().toString();
        repeatPass = etRepeatPass.getText().toString();
        name = etName.getText().toString();
        lastname = etLastname.getText().toString();
        //contact info
        email = etEmail.getText().toString();
        phone = "";
        phone = etPhone.getText().toString();
        mobile = "";
        mobile = etMobile.getText().toString();
        if(phone.length()<7 && !phone.equals("")){
            phone = "";
            etPhone.setText("");
            Toast.makeText(getApplicationContext(), "Phone number is too short!", Toast.LENGTH_LONG).show();
            return;
        }
        if(mobile.length()<10 && !mobile.equals("")){
            etMobile.setText("");
            Toast.makeText(getApplicationContext(), "Mobile number is too short!", Toast.LENGTH_LONG).show();
            return;
        }
        if (name.equals("") || lastname.equals("") || email.equals("") || pass.equals("") || repeatPass.equals("")) {
            Toast.makeText(getApplicationContext(), "Fill all required fields", Toast.LENGTH_LONG).show();
	    		return;
        } else {

            if (pass.length() >= 6) {
                if (pass.equals(repeatPass)) {
                    new registerUser().execute();
                }else {
                    Toast.makeText(getApplicationContext(), "Passwords don't match!", Toast.LENGTH_LONG).show();
                    etPass.setText("");
                    etRepeatPass.setText("");
                    return;
                }
            } else {
                Toast.makeText(getApplicationContext(), "Password length must be over 6 characters!", Toast.LENGTH_LONG).show();
                return;
            }
        }
    }

    /**
     * This class extends AsynTask and provide asynchronous http request
     */
    private class registerUser extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Registration.this);
            pDialog.setMessage("Registering user. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", pass));
            params.add(new BasicNameValuePair("name", name));
            params.add(new BasicNameValuePair("lastname", lastname));
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("phone", phone));
            params.add(new BasicNameValuePair("mobile", mobile));

            JSONParser jsonparser = new JSONParser();
            JSONObject jsonObject = jsonparser.makeHttpRequest(global.getHost()+registerUrl, "POST", params);
            Log.e("SCF", "Do ovdje!!!!!!!!!!!!!!!!!!!!!!!!!!");
            try {
                String queryStatus = jsonObject.getString("query");
                String mailStatus = jsonObject.getString("mail");

                if (jsonObject.getBoolean("username")) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            // ako je query false onda, upis nije uspjesan
                            Toast.makeText(getApplicationContext(), "Username or email already exists!", Toast.LENGTH_LONG).show();
                            etUsername.setText("");
                            etEmail.setText("");
                        }
                    });

                } else {
                    if (mailStatus.equals("false")) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                // ako je mail los onda vrati mail false
                                Toast.makeText(getApplicationContext(), "False email address", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                //ako je tocan reg je prosla
                                Toast.makeText(getApplicationContext(), "You're registered, check e-mail", Toast.LENGTH_LONG).show();
                                try {
                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                //return to main activity
                                finish();
                            }
                        });
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting travel info
            pDialog.dismiss();

            finishActivity(0);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


}

