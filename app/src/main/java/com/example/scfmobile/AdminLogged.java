package com.example.scfmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import static android.widget.Toast.*;


public class AdminLogged extends Activity {

    GlobalValues global;
    private String username = "";
    SessionManager manager;
    MultiSelectionSpinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_logged);
        manager = new SessionManager(getApplicationContext());
        global = (GlobalValues) getApplicationContext();


        if(!isNetworkConnected()){
            new AlertDialog.Builder(this)
                    .setTitle("NETWORK")
                    .setMessage("Check your network connection!")
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue
                            finish();
                        }
                    })
                    .show();
        }
    }

    public void onClick(View v){
        String s = spinner.getSelectedItemsAsString();
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    public void manageCenters(View clickedButton) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), SearchAdmin.class);
        startActivity(intent);
    }

    public void manageReports(View clickedButton) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), Reports.class);
        startActivity(intent);
    }

    public void manageUsers(View clickedButton) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), SearchUsers.class);
        startActivity(intent);
    }

    public void changePass(View view) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), ChangePass.class);
        intent.putExtra("username", manager.getUsername());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onDestroy();

        Toast.makeText(getApplicationContext(), manager.getUsername()+" you're logged out!!", Toast.LENGTH_LONG).show();
        manager.logoutUser();
        startActivity(new Intent(this, Main.class));
        finish();

    }

}
