package com.example.scfmobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Profile extends Activity {
    SessionManager manager;
    GlobalValues global;
    String updateUrl;
    String userUrl;
    String urlChangePass;
    TextView tvUsername, tvEmail;
    private EditText etUsername, etDeletePass, etName, etLastname, etPhone, etMobile, etPass, etRepeatPass, etOldPass;// etTown, etStreet, etPost, etState;
    private String username, deletePass, email, mobile, pass, phone, repeatPass, name, lastname, oldPass;// town, street, post, state;
    private ProgressDialog pDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        global = (GlobalValues) getApplicationContext();
        manager = new SessionManager(getApplicationContext());
        new GetUserInfo().execute(manager.getUsername());

        etName = (EditText) findViewById(R.id.newUserName);
        etLastname = (EditText) findViewById(R.id.newUserLastname);
        etPhone = (EditText) findViewById(R.id.phone_num);
        etMobile = (EditText) findViewById(R.id.mobile_num);
        etPass = (EditText) findViewById(R.id.new_password);
        etRepeatPass = (EditText) findViewById(R.id.repeat_password);
        etOldPass = (EditText) findViewById(R.id.old_password);

        tvEmail = (TextView) findViewById(R.id.email);
        tvUsername = (TextView) findViewById(R.id.username);
        tvUsername.setText(manager.getUsername());
        etDeletePass = (EditText) findViewById(R.id.password_del);




    }

    public void delete (View view){
        deletePass = etDeletePass.getText().toString();
        if (deletePass.length() != 0) {
            new AccountDeleter().execute(deletePass);
        } else {
            Toast.makeText(getApplicationContext(), "You need to write your password!", Toast.LENGTH_LONG).show();
        }
    }

    public void update(View clickedButton) {
        //user info
        username = manager.getUsername();
        name = etName.getText().toString();
        lastname = etLastname.getText().toString();
        //contact info
        phone = "";
        phone = etPhone.getText().toString();
        mobile = "";
        mobile = etMobile.getText().toString();
        //contact info
        if (phone.length() < 7 && !phone.equals("")) {
            phone = "";
            etPhone.setText("");
            Toast.makeText(getApplicationContext(), "Phone number is too short!", Toast.LENGTH_LONG).show();
            return;
        }
        if (mobile.length() < 10 && !mobile.equals("")) {
            mobile = "";
            etMobile.setText("");
            Toast.makeText(getApplicationContext(), "Mobile number is too short!", Toast.LENGTH_LONG).show();
            return;
        }
        if (name.equals("") || lastname.equals("")) {
            Toast.makeText(getApplicationContext(), "Fill all required fields", Toast.LENGTH_LONG).show();
//	    		return;
        } else {
            new UpdateUserInfo().execute();
        }
    }

    public void changePass(View view) {
        pass = etPass.getText().toString();
        repeatPass = etRepeatPass.getText().toString();
        oldPass = etOldPass.getText().toString();


        if (oldPass.equals("") || pass.equals("") || repeatPass.equals("")) {
            Toast.makeText(getApplicationContext(), "Fill all passwords!", Toast.LENGTH_SHORT).show();
        } else {
            if (pass.length() >= 6) {
                if (pass.equals(repeatPass)) {
                    new PasswordChanger().execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Passwords don't match!", Toast.LENGTH_LONG).show();
                    etPass.setText("");
                    etRepeatPass.setText("");
                }
            } else {
                Toast.makeText(getApplicationContext(), "Password length must be over 6 characters!", Toast.LENGTH_LONG).show();
            }
        }
    }

    class GetUserInfo extends AsyncTask<String, Void, Void> {
        JSONParser jsonParser = new JSONParser();

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", manager.getUsername()));

            final JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost() + "getUserInfo.php", "POST", params);

            Log.d("JSON", jsonObject.toString());

            try {
                if (jsonObject.getBoolean("status")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                etName.setText(jsonObject.getString("name"));
                                etLastname.setText(jsonObject.getString("lastname"));
                                tvEmail.setText(jsonObject.getString("email"));
                                etMobile.setText(jsonObject.getString("mobile"));
                                etPhone.setText(jsonObject.getString("phone"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    class PasswordChanger extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            Log.d("PARAMS", manager.getUsername() + " " + oldPass);
            params.add(new BasicNameValuePair("username", manager.getUsername()));
            params.add(new BasicNameValuePair("password", pass));
            params.add(new BasicNameValuePair("old_password", oldPass));
            JSONParser jsonParser = new JSONParser();
            final JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost() + "changePassword.php", "POST", params);
            Log.d("JSON", jsonObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (jsonObject.getBoolean("oldPass")) {
                            if (jsonObject.getBoolean("query"))
                                Toast.makeText(getApplicationContext(), "Password changed!", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Incorrect old password!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }
    }

    /**
     * This class extends AsynTask and provide asynchronous http request
     */
    private class UpdateUserInfo extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("name", name));
            params.add(new BasicNameValuePair("lastname", lastname));
            params.add(new BasicNameValuePair("username", manager.getUsername()));
            params.add(new BasicNameValuePair("phone", phone));
            params.add(new BasicNameValuePair("mobile", mobile));

            JSONParser jsonparser = new JSONParser();
            JSONObject jsonObject = jsonparser.makeHttpRequest(global.getHost() + "changeUserData.php", "POST", params);
            Log.d("JSON", jsonObject.toString());
            try {
                Boolean queryStatus = jsonObject.getBoolean("query");

                if (!queryStatus) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            // ako je query false onda, upis nije uspjesan
                            Toast.makeText(getApplicationContext(), "Update failed!", Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            // ako je mail los onda vrati mail false
                            Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting travel info
        }

    }

    class AccountDeleter extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", manager.getUsername()));
            params.add(new BasicNameValuePair("password", strings[0]));
            Log.d("PARAMS", manager.getUsername()+" "+strings[0]);
            JSONParser jsonparser = new JSONParser();
            JSONObject jsonObject = jsonparser.makeHttpRequest(global.getHost() + "deactivateUser.php", "POST", params);
            try {
                boolean query =jsonObject.getBoolean("query");
                if (query){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            // ako je mail los onda vrati mail false
                            Toast.makeText(getApplicationContext(), "You deleted your account!", Toast.LENGTH_SHORT).show();
                            manager.logoutUser();
                            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            // ako je query false onda, upis nije uspjesan
                            Toast.makeText(getApplicationContext(), "Delete failed!", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
    }

}