package com.example.scfmobile;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class ReportListDisplayAdapter extends ArrayAdapter<Report> {

    WindowManager wm;
    SessionManager manager;
    private List<Report> items;
    GlobalValues global;
    Context currentContext;

    public ReportListDisplayAdapter(Context context, int resource, List<Report> items, WindowManager _wm) {

        super(context, resource, items);

        this.items = items;
        this.wm = _wm;
        this.manager = new SessionManager(context);
        this.global = (GlobalValues) context.getApplicationContext();
        this.currentContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {

            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.reports_display_item, null);

        }

        Report p = items.get(position);

        if (p != null) {


            TextView tvCenter = (TextView) v.findViewById(R.id.tvCenter);
            TextView tvSuggestion = (TextView) v.findViewById(R.id.tvSuggestion);
            TextView tvTime = (TextView) v.findViewById(R.id.tvTime);
            TextView tvUsername = (TextView) v.findViewById(R.id.tvUsername);
            TextView tvCenterId = (TextView) v.findViewById(R.id.tvCenterId);
            TextView tvProblem = (TextView) v.findViewById(R.id.tvProblem);
            TextView tvErrorId = (TextView) v.findViewById(R.id.tvErrorId);
            TextView tvStatus = (TextView) v.findViewById(R.id.tvStatus);
            TextView tvEmail = (TextView) v.findViewById(R.id.tvEmail);

            if (tvCenter != null) {
                tvCenter.setText(p.getCenterName());
            }
            if (tvTime != null) {
                tvTime.setText(p.getTime());
            }
            if (tvErrorId != null) {
                tvErrorId.setText(p.getErrorId());
            }
            if (tvSuggestion != null) {
                tvSuggestion.setText(p.getSuggestion());
            }

            if (tvUsername != null) {
                tvUsername.setText(p.getUsername());
            }
            if (tvCenterId != null) {
                tvCenterId.setText(p.getCenterId());
            }
            if (tvProblem != null) {
                tvProblem.setText(p.getProblem());
            }
            if (tvStatus != null) {
                tvStatus.setText(p.getStatus());
            }
            if (tvEmail != null) {
                tvEmail.setText(p.getEmail());
            }
        }

        return v;

    }
}
