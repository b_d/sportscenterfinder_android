package com.example.scfmobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.scfmobile.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChangePass extends Activity {
    GlobalValues global;
    EditText etPass, etRepeat, etOldPass;
    Button change;
    String pass, repeatPass, oldPass, username;
    private ProgressDialog pDialog;
    SessionManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        Intent intent;
        intent = getIntent();
        username = intent.getStringExtra("username");
        global = (GlobalValues) getApplicationContext();

        etPass = (EditText)findViewById(R.id.etNewPass);
        etRepeat = (EditText)findViewById(R.id.etRepeatPass);
        etOldPass = (EditText)findViewById(R.id.etOldPass);

        change = (Button)findViewById(R.id.btnChange);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pass = etPass.getText().toString();
                repeatPass = etRepeat.getText().toString();
                oldPass = etOldPass.getText().toString();

                if (!pass.equals("") && !repeatPass.equals("") && !oldPass.equals("")) {
                    if (pass.length() >= 6) {
                        if (pass.equals(repeatPass)) {
                            pDialog = new ProgressDialog(ChangePass.this);
                            pDialog.show();
                            new change().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), "Passwords don't match!", Toast.LENGTH_LONG).show();
                            etPass.setText("");
                            etRepeat.setText("");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Password length must be over 6 characters!", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Enter data to all fields!!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void check_change(Integer s){
        if(s == 1){
            Toast.makeText(ChangePass.this, "Promjenjen password, prilikom slijedeceg logina unesitte novi password!", Toast.LENGTH_SHORT).show();
            finish();

        }else if (s == -1){
            Toast.makeText(ChangePass.this, "Unjeli ste neispravnu staru lozinku!!", Toast.LENGTH_SHORT).show();
        }
    }

    private class change extends AsyncTask<Void, Void, Void> {
        private Integer status;

        @Override
        protected void onPostExecute(Void aVoid) {

            pDialog.dismiss();
            check_change(status);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("oldPass", oldPass));
            param.add(new BasicNameValuePair("newPass", pass ));
            param.add(new BasicNameValuePair("username", username ));

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"changePassword2.php", "POST", param);
            Log.d("Response", jsonObject.toString());
            try {
                Boolean oldPass = jsonObject.getBoolean("oldPass");
                Boolean query = jsonObject.getBoolean("query");
                if (oldPass == true) {
                    if(query == true){
                        status = 1;
                    }
                }else{
                    status = -1;
                }
            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }

            return null;
        }
    }

}
