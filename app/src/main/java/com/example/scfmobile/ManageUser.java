package com.example.scfmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ManageUser extends Activity {

    GlobalValues global;
    SessionManager manager;
    MultiSelectionSpinner spinner;
    public String username, user, name, surname, email, phone, mobile, date_added, last_login, verify, status;
    Button btnUpdate;
    public CheckBox cbStatus;
    public TextView tvUsername, tvName, tvSurname, tvEmail, tvPhone, tvMobile, tvDateAdded, tvLastLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_user);
        manager = new SessionManager(getApplicationContext());
        global = (GlobalValues) getApplicationContext();
        spinner = (MultiSelectionSpinner) findViewById(R.id.mySpinner1);

        if(!isNetworkConnected()){
            new AlertDialog.Builder(this)
                    .setTitle("NETWORK")
                    .setMessage("Check your network connection!")
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue
                            finish();
                        }
                    })
                    .show();
        }
        InitData();
        new getUserInfo().execute();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    private void InitData() {
        Intent intent;
        intent = getIntent();
        user = intent.getStringExtra("username");
        tvUsername = (TextView)findViewById(R.id.tvUsername);
        tvName = (TextView)findViewById(R.id.tvName);
        tvSurname = (TextView)findViewById(R.id.tvSurname);
        tvEmail = (TextView)findViewById(R.id.tvEmail);
        tvPhone = (TextView)findViewById(R.id.tvPhone);
        tvMobile = (TextView)findViewById(R.id.tvMobile);
        tvDateAdded = (TextView)findViewById(R.id.tvDateAdded);
        tvLastLogin = (TextView)findViewById(R.id.tvLastLogin);

        btnUpdate = (Button)findViewById(R.id.btnUpdate);
        cbStatus = (CheckBox) findViewById(R.id.cbStatus);

        cbStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(cbStatus.isChecked()){
                    verify = "0";
                }else{
                    verify = "1";
                }
                new UpdateUser().execute();
            }
        });

    }
    private  void UpdateText(){
        tvUsername.setText(username);
        tvName.setText(name);
        tvSurname.setText(surname);
        tvEmail.setText(email);
        tvPhone.setText(phone);
        tvMobile.setText(mobile);
        tvDateAdded.setText(date_added);
        tvLastLogin.setText(last_login);

        if (verify.equals("0")){
            cbStatus.setChecked(true);
        }
    }
    private class getUserInfo extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            UpdateText();
        }


        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("username", user));

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"getUserInformation.php", "POST", param);
            Log.d("Response", jsonObject.toString());

            try {

                username = jsonObject.getString("username");
                name = jsonObject.getString("name");
                surname = jsonObject.getString("surname");
                email = jsonObject.getString("email");
                phone = jsonObject.getString("phone");
                mobile = jsonObject.getString("mobile");
                date_added = jsonObject.getString("date_added");
                last_login = jsonObject.getString("last_login");
                verify = jsonObject.getString("verify");

            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }
            return null;
        }
    }
    private class UpdateUser extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("username",username));
            params.add(new BasicNameValuePair("verify",verify));

            JSONParser jsonparser = new JSONParser();
            JSONObject jsonObject = jsonparser.makeHttpRequest(global.getHost() + "updateUser.php", "GET", params);
            Log.d("JSON", jsonObject.toString());
            try {
                Boolean queryStatus = jsonObject.getBoolean("query");

                if (!queryStatus) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Update failed!", Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

    }

}
