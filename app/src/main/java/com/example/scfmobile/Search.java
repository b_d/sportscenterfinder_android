package com.example.scfmobile;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Search extends Activity {

    //Globals
    GlobalValues global;
    SessionManager manager;
    String TAG = "Unllogged search TAG";
    String ERROR = "Unllogged search ERROR";
    private static String url = "http://10.0.2.2/scf2DBManage/getActivites.php";
    //Search params
    String name, town, county, street, country, availableActivity, sort;
    EditText etName, etStreet, etTown, etCounty, etCountry;
    RadioGroup rgSort;
    Spinner sp;
    List<String> activities;
    List<String> ids;
    public ArrayAdapter<String> listAdapter;

    ArrayList<ArrayList<String>> centersList;


    //JSON TAGs
    String STATUS_TAG="status";
    String CENTERS_TAG="centers";
    String NAME_TAG="name";
    String ID_TAG="center_id";
    String LAT_TAG="lat";
    String LNG_TAG="lng";
    String STREET_TAG="street";
    String TOWN_TAG="town";
    String COUNTY_TAG="county";
    String COUNTRY_TAG="country";
    String ACTIVITIES_TAG="activities";
    String ACTIVITY_ID_TAG="activity_id";
    String ACTIVITY_TAG="activity";
    String RATING_TAG="rating";
    String POST_TAG="post";
    String DESCRIPTION_TAG="description";
    String ADDED_BY_USER_TAG="added_by_user";
    String MAP_URL_TAG="url";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_menu);
        global = (GlobalValues) getApplicationContext();
        manager = new SessionManager(getApplicationContext());
        
        etInit();
        new ActivitiesGetter().execute();
    }

    private void etInit() {

        etName = (EditText) findViewById(R.id.input_name);
        etStreet = (EditText) findViewById(R.id.input_street);
        etTown = (EditText) findViewById(R.id.input_town);
        etCounty = (EditText) findViewById(R.id.input_county);
        etCountry = (EditText) findViewById(R.id.input_country);
        rgSort = (RadioGroup) findViewById(R.id.input_sort);
        sp = (Spinner) findViewById(R.id.input_activites);
        activities = new ArrayList<String>();
        ids = new ArrayList<String>();
        listAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, activities);
        sp.setAdapter(listAdapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                availableActivity=ids.get(arg2);
//                Toast.makeText(getApplicationContext(), availableActivity, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
                availableActivity="";

            }
        });

        //centers
        centersList = new ArrayList<ArrayList<String>>();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.unlogged_search, menu);
        return true;
    }

    public void search(View clickedButton) {
        //default value
        name = street = town = county = country = "";
//        availableActivity = "";
        //center info
        name = etName.getText().toString();
        street = etStreet.getText().toString();
        town = etTown.getText().toString();
        county = etCounty.getText().toString();
        country = etCountry.getText().toString();

        //defining sort type
        if (R.id.ndesc == rgSort.getCheckedRadioButtonId()) {
            sort = "ndesc";
        } else if (R.id.gasc == rgSort.getCheckedRadioButtonId()) {
            sort = "gasc";
        } else if (R.id.gdesc == rgSort.getCheckedRadioButtonId()) {
            sort = "gdesc";
        } else {
            sort = "nasc";
        }

        new getCenters().execute();

    }


    class getCenters extends AsyncTask<String, String, String> {

        ArrayList<ArrayList<String>> tempCentersList = new ArrayList<ArrayList<String>>();

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "uslo");
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("name", name));
            params.add(new BasicNameValuePair("street", street));
            params.add(new BasicNameValuePair("town", town));
            params.add(new BasicNameValuePair("county", county));
            params.add(new BasicNameValuePair("country", country));
            params.add(new BasicNameValuePair("availableActivity", availableActivity));
            params.add(new BasicNameValuePair("sort", sort));
            Log.i(TAG, "obradilo tagove");

            jsonObject = jsonParser.makeHttpRequest(global.getHost()+"searchEngineJSON.php", "GET", params);

//            Log.d("Response", jsonObject.toString());
            try {

                if (jsonObject.getInt(STATUS_TAG) == 100) {
                    JSONArray centers = jsonObject.getJSONArray(CENTERS_TAG);

                    ArrayList<String> currentCenterInfo = new ArrayList<String>();
                    for (int i=0; i<centers.length();i++){
                        JSONObject currCenter=centers.getJSONObject(i);

                        currentCenterInfo.clear();

                        currentCenterInfo.add(currCenter.getString(NAME_TAG));
                        currentCenterInfo.add(currCenter.getString(ID_TAG));
                        currentCenterInfo.add(currCenter.getString(STREET_TAG));
                        currentCenterInfo.add(currCenter.getString(POST_TAG));
                        currentCenterInfo.add(currCenter.getString(TOWN_TAG));
                        currentCenterInfo.add(currCenter.getString(COUNTY_TAG));
                        currentCenterInfo.add(currCenter.getString(COUNTRY_TAG));
                        currentCenterInfo.add(currCenter.getString(LAT_TAG));
                        currentCenterInfo.add(currCenter.getString(LNG_TAG));
                        currentCenterInfo.add(currCenter.getString(ADDED_BY_USER_TAG));
                        currentCenterInfo.add(currCenter.getString(ACTIVITIES_TAG));
                        Log.d(ACTIVITIES_TAG,currCenter.getString(ACTIVITIES_TAG));
                        currentCenterInfo.add(currCenter.getString(RATING_TAG));
                        currentCenterInfo.add(currCenter.getString(DESCRIPTION_TAG));


                        tempCentersList.add(currentCenterInfo);
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            centersList.clear();
            centersList.equals(tempCentersList);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent();
                    i.setClass(getApplicationContext(), CenterList.class);
                    i.putExtra("centers_list",jsonObject.toString());
                    startActivity(i);

                }
            });
        }
    }


    /**
     * \brief ActivitiesGetter gets all activity info from DB
     *
     */

    private class ActivitiesGetter extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {

            listAdapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"getActivites.php", "GET", null);
            Log.d("Response", jsonObject.toString());
            try {
                if (jsonObject.getInt(STATUS_TAG) == 100) {
                    JSONArray ja = jsonObject.getJSONArray(ACTIVITIES_TAG);
                    activities.clear();
                    activities.add("");
                    ids.clear();
                    ids.add("");
                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jObj = ja.getJSONObject(i);
                        ids.add(jObj.getString(ACTIVITY_ID_TAG));
                        activities.add(jObj.getString(ACTIVITY_TAG));
                        Log.i("ID:", jObj.getString(ACTIVITY_ID_TAG));
                        Log.i("ACTIVITY:",jObj.getString(ACTIVITY_TAG));
                    }
                }
            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }
            return null;
        }
    }
}
