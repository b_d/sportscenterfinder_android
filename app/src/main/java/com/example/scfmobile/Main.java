package com.example.scfmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is entry activity for this application
 */
public class Main extends Activity {


    ///Variables
    //Widget variable (et - eDITtEXT, tv - tEXTvIEW, btn - bUtTOn)
    EditText etUName;
    EditText etPWord;
    Button btnLogin;
    Button btnSignUp;
    Button btnSearch;
    Button btnRecover;
    // Progress Dialog
    private ProgressDialog pDialog;
    AlertDialog aDialog;
    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();
    SessionManager manager;
    GlobalValues global;
    //defining response events 
    public static final String TAG_STATUS = "status";
    public static final String TAG_USERNAME = "username";
    public static final String TAG_PASSWORD = "password";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isNetworkConnected()){
            new AlertDialog.Builder(this)
                    .setTitle("NETWORK")
                    .setMessage("Check your network connection!")
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue
                            finish();
                        }
                    })
                    .show();
        }

        manager = new SessionManager(getApplicationContext());
        manager.checkLoginOnMain();

        global = (GlobalValues) getApplicationContext();

        intialize();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    /**
     * This method will set and define activity ui items
     */
    private void intialize() {
        etUName = (EditText) findViewById(R.id.username_input);
        etPWord = (EditText) findViewById(R.id.password_input);
        btnLogin = (Button) findViewById(R.id.log_in_btn);
        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    if(etUName.getText().toString().equals("") && etPWord.getText().toString().equals("")){
                        Toast.makeText(getApplicationContext(),"Enter username and password!", Toast.LENGTH_SHORT).show();
                    }else{
                        Login login = new Login();
                        login.execute();
                    }
                } catch (Exception e) {
                    // TODO: handle exceptiong
                    Log.e("D", "Unable to start login!");
                }
            }
        });

        btnSignUp = (Button) findViewById(R.id.register_btn);
        btnSignUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(getApplicationContext(), Registration.class);
                startActivity(i);
            }
        });

        btnSearch = (Button) findViewById(R.id.unlloged_search_btn);
        btnSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(getApplicationContext(), Search.class);
                startActivity(i);
            }
        });

        btnRecover = (Button) findViewById(R.id.recover_btn);
        btnRecover.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(getApplicationContext(), Recover.class);
                startActivity(i);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Background Async Task to login to app
     */
    class Login extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Main.this);
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating http request for login
         */
        protected String doInBackground(String... args) {
            final String username = etUName.getText().toString();
            final String password = etPWord.getText().toString();

            JSONObject json = null;
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));

            // getting JSON Object
            // Note that create product url accepts POST method
            Log.d("URL", global.getHost()+"login.php");
            json = jParser.makeHttpRequest(global.getHost()+"login.php", "POST", params);

            // check log cat for response
            Log.d("JSON", json.toString());

            // check for success tag
            try {

                Integer status = json.getInt(TAG_STATUS);
                if (status == 1) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            manager.createLoginSession(username);

                            Toast.makeText(getApplicationContext(), username+" you're logged in!", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent();
                            i.setClass(Main.this, AdminLogged.class);
                            startActivity(i);
//                            finish();
                        }
                    });

//                    finish();
                } else if(status == 2){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            manager.createLoginSession(username);

                            Toast.makeText(getApplicationContext(), username+" you're logged in!", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent();
                            i.setClass(Main.this, Logged.class);
                            startActivity(i);
//                            finish();
                        }
                    });

//                    finish();
                } else {
                    Log.d("D", "Ne prolazi username i pass dio");
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            Toast.makeText(getApplicationContext(), "Wrong username or password!", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            } catch (JSONException e) {
                Log.e("JSON exception", e.toString());
                e.printStackTrace();
            } catch (Exception e) {
                Log.e("Exception", e.toString());
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
        }

    }

    @Override
    public void onBackPressed() {
        super.onDestroy();
        finish();

    }
}
