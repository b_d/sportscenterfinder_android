package com.example.scfmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Logged extends Activity {

    private String username = "";
    SessionManager manager;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(), manager.getUsername()+" you're logged out!", Toast.LENGTH_LONG).show();
        manager.logoutUser();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged);
        manager = new SessionManager(getApplicationContext());

        if(!isNetworkConnected()){
            new AlertDialog.Builder(this)
                    .setTitle("NETWORK")
                    .setMessage("Check your network connection!")
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue
                            finish();
                        }
                    })
                    .show();
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }


    public void setSearch(View clickedButton) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), Search.class);
        startActivity(intent);

    }

    public void setNew(View clickedButton) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), NewCenter.class);
        startActivity(intent);

    }

    public void setProfile(View view) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), Profile.class);
        startActivity(intent);
    }

    public void changePass(View view) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), ChangePass.class);
        intent.putExtra("username", manager.getUsername());
        startActivity(intent);
    }
}
