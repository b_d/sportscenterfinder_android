package com.example.scfmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ManageCenters extends Activity {

    GlobalValues global;
    SessionManager manager;
    MultiSelectionSpinner spinner;
    public String center_id, center, id;
    Button btnUpdate;
    List<List<String>> commentList;
    ListView lvComments;
    CommentsListAdapter adapter;

    public EditText etName,etCountry,etCounty, etStreet, etTown, etPost_num, etLat, etLng, etDesc, etRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_centers);
        manager = new SessionManager(getApplicationContext());
        global = (GlobalValues) getApplicationContext();
        spinner = (MultiSelectionSpinner) findViewById(R.id.mySpinner1);

        if(!isNetworkConnected()){
            new AlertDialog.Builder(this)
                    .setTitle("NETWORK")
                    .setMessage("Check your network connection!")
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue
                            finish();
                        }
                    })
                    .show();
        }
        InitData();
        new getCenterInfo().execute();
        new ActivitiesGetter().execute();
        new CommentsGetter().execute(id);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdateCenterInfo().execute();
            }
        });


        Toast.makeText(getApplicationContext(), center, Toast.LENGTH_SHORT).show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    private void InitData() {
        Intent intent;
        intent = getIntent();
        center = intent.getStringExtra("center");
        id = intent.getStringExtra("center_id");
        etName = (EditText)findViewById(R.id.etName);
        etCounty = (EditText)findViewById(R.id.etCounty);
        etCountry = (EditText)findViewById(R.id.etCountry);
        etStreet = (EditText)findViewById(R.id.etStreet);
        etTown = (EditText)findViewById(R.id.etTown);
        etPost_num = (EditText)findViewById(R.id.etPost_num);
        etLat = (EditText)findViewById(R.id.etLat);
        etLng = (EditText)findViewById(R.id.etLng);
        etDesc = (EditText)findViewById(R.id.etDesc);
        etDesc.setMovementMethod(ScrollingMovementMethod.getInstance());
        etRating = (EditText)findViewById(R.id.etRating);

        btnUpdate = (Button)findViewById(R.id.btnUpdate);
        commentList = new ArrayList<List<String>>();

    }

    private class getCenterInfo extends AsyncTask<Void, Void, Void> {
        String county, country, street, town, post_num, lat, lng, desc, rating;

        @Override
        protected void onPostExecute(Void aVoid) {

            etName.setText(center);
            etCounty.setText(county);
            etCountry.setText(country);
            etStreet.setText(street);
            etTown.setText(town);
            etPost_num.setText(post_num);
            etLat.setText(lat);
            etLng.setText(lng);
            etDesc.setText(desc);
            etRating.setText(rating);

            //listAdapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("id", id));

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"getCenter.php", "GET", param);
            Log.d("Response", jsonObject.toString());
            try {
                center = jsonObject.getString("name");
                county = jsonObject.getString("county");
                country = jsonObject.getString("country");
                street = jsonObject.getString("street");
                town = jsonObject.getString("town");
                post_num = jsonObject.getString("post");
                lat = jsonObject.getString("lat");
                lng = jsonObject.getString("lng");
                desc = jsonObject.getString("description");
                rating = jsonObject.getString("rating");

            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }
            return null;
        }
    }

    private class UpdateCenterInfo extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            Log.d("ASD", etDesc.getText().toString() );
            params.add(new BasicNameValuePair("center_id", id));
            params.add(new BasicNameValuePair("name", etName.getText().toString()));
            params.add(new BasicNameValuePair("county", etCounty.getText().toString()));
            params.add(new BasicNameValuePair("country", etCountry.getText().toString()));
            params.add(new BasicNameValuePair("street", etStreet.getText().toString()));
            params.add(new BasicNameValuePair("town", etTown.getText().toString()));
            params.add(new BasicNameValuePair("post", etPost_num.getText().toString()));
            params.add(new BasicNameValuePair("lat", etLat.getText().toString()));
            params.add(new BasicNameValuePair("lng", etLng.getText().toString()));
            params.add(new BasicNameValuePair("description", etDesc.getText().toString()));
            params.add(new BasicNameValuePair("rating", etRating.getText().toString()));
            params.add(new BasicNameValuePair("center_activities", spinner.getSelectedItemsAsString()));

            JSONParser jsonparser = new JSONParser();
                JSONObject jsonObject = jsonparser.makeHttpRequest(global.getHost() + "changeCenterData.php", "GET", params);
            Log.d("JSON", jsonObject.toString());
            try {
                Boolean queryStatus = jsonObject.getBoolean("query");

                if (!queryStatus) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            // ako je query false onda, upis nije uspjesan
                            Toast.makeText(getApplicationContext(), "Update failed!", Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            // ako je mail los onda vrati mail false
                            Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting travel info
            //recreate();
            new getCenterInfo().execute();

        }

    }

    class ActivitiesGetter extends AsyncTask<Void, Void, Void> {

        ArrayList<String> activities_list = new ArrayList<String>();
        ArrayList<Integer> center_activity_ids = new ArrayList<Integer>();

        @Override
        protected void onPostExecute(Void aVoid) {

            String[] array = new String[activities_list.size()];
            Integer[] array2 = new Integer[center_activity_ids.size()];
            array = activities_list.toArray(array);
            array2 = center_activity_ids.toArray(array2);

            spinner.setItems(array, array2);

        }

        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("center_id", id));

            JSONParser jsonparser = new JSONParser();
            JSONObject jobj = jsonparser.makeHttpRequest(global.getHost() + "getActivites2.php", "GET", params);

            Log.d("Response", jobj.toString());
            try {

                JSONArray ja = jobj.getJSONArray("activities");

                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jObj = ja.getJSONObject(i);
                    Log.i("ACTIVITY:",jObj.getString("activity"));
                    activities_list.add(jObj.getString("activity"));
                }
                ja = jobj.getJSONArray("center_activities");
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jObj = ja.getJSONObject(i);
                    Log.i("CENTER ACTIVITY:",jObj.getString("id"));
                    center_activity_ids.add(jObj.getInt("id"));
                }
            } catch (JSONException e) {
                Log.e("JSONException", "JSONException: " + e.toString());
            }
            return null;
        }
    }

    class CommentsGetter extends AsyncTask<String, Void, String>{

        protected void onPostExecute(String file_url) {

            lvComments = (ListView) findViewById(R.id.comment_list_admin);
            lvComments.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });
            adapter = new CommentsListAdapter(getApplicationContext(), R.layout.comment_item,commentList);
            lvComments.setAdapter(adapter);

        }
        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id", strings[0]));
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"generateComments.php", "GET", params);
            try {
                if(jsonObject.getInt("status")==100){
                    //centerId = jsonObject.getString("center_id");
                    JSONArray centers = jsonObject.getJSONArray("comments");
                    commentList.clear();
                    for (int i = 0; i < centers.length(); i++) {
                        List<String> commentAttrs = new ArrayList<String>();
                        JSONObject comment = centers.getJSONObject(i);

                        commentAttrs.clear();

                        commentAttrs.add(comment.optString("comment_id"));
                        commentAttrs.add(comment.optString("comment"));
                        commentAttrs.add(comment.optString("by_user"));
                        commentAttrs.add(comment.optString("time"));
                        commentList.add(commentAttrs);
                    }
                }else if(jsonObject.getInt("status")== 101){
                    //zero comments

                }else if(jsonObject.getInt("status") == 102){
                    //error


                }
            }catch (JSONException e){
                Log.e("JSON", e.toString());
            }

            return null;
        }

    }

    class CommentsListAdapter extends ArrayAdapter<List<String>> {
        List<List<String>> comments;
        SessionManager manager;
        public CommentsListAdapter(Context context, int textViewResourceId, List<List<String>> objects) {
            super(context, textViewResourceId, objects);
            this.comments = objects;
            manager = new SessionManager(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.comment_item, null);
            }

            TextView tvHeader = (TextView) v.findViewById(R.id.comment_header);
            TextView tvBody = (TextView) v.findViewById(R.id.comment_body);
            Button btn = (Button) v.findViewById(R.id.delete_comment);

            List<String> comment = comments.get(position);

            if (tvHeader != null){
                tvHeader.setText("Added by: \""+ comment.get(2)+"\" at "+comment.get(3));

            }
            if (tvBody!= null){
                tvBody.setText("Comment: "+comment.get(1) );
            }
            if (btn!=null){

                final String tmpCommentId = new String(comment.get(0));
                final String tmpComment = new String(comment.get(1));
                final String tmpCommentTime = new String(comment.get(3));
                final String pos = String.valueOf(position);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new CommentDeleter().execute(tmpCommentId, tmpComment, tmpCommentTime, pos);
                    }
                });
            }

            return v;
        }
    }

    class CommentDeleter extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("comment_id",strings[0]));
            params.add(new BasicNameValuePair("comment",strings[1]));
            params.add(new BasicNameValuePair("time",strings[2]));
            params.add(new BasicNameValuePair("user", manager.getUsername()));
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = jsonParser.makeHttpRequest(global.getHost()+"deleteComment.php", "GET", params);
            Log.d("JSON", jsonObject.toString());
            try {
                if(jsonObject.getBoolean("query")){
                    //ok
                    final String position = new String(strings[3]);
//                    final String str1 = new String(strings[1]);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),"Comment deletedddd!", Toast.LENGTH_LONG).show();
                            new CommentsGetter().execute(id);
                        }
                    });
                }else {
                    //zero comments

                }
            }catch (JSONException e){
                Log.e("JSON", e.toString());
            }

            return null;
        }
    }
}
