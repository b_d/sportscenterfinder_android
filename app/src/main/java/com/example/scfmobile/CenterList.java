package com.example.scfmobile;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CenterList extends Activity {


    WindowManager wm;
    String STATUS_TAG = "status";
    String CENTERS_TAG = "centers";
    String NAME_TAG = "name";
    String ID_TAG = "center_id";
    String LAT_TAG = "lat";
    String LNG_TAG = "lng";
    String STREET_TAG = "street";
    String TOWN_TAG = "town";
    String COUNTY_TAG = "county";
    String COUNTRY_TAG = "country";
    String ACTIVITIES_TAG = "activities";
    String ACTIVITY_ID_TAG = "activity_id";
    String ACTIVITY_TAG = "activity";
    String RATING_TAG = "rating";
    String POST_TAG = "post";
    String DESCRIPTION_TAG = "description";
    String ADDED_BY_USER_TAG = "added_by_user";
    String MAP_URL_TAG = "url";
    List<Item> centersList;
    SearchListDisplayAdapter adapter;
    ListView lv;
    SessionManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_center_list);

        manager = new SessionManager(getApplicationContext());

        wm = getWindowManager();
        centersList = new ArrayList<Item>();
        lv = (ListView) findViewById(R.id.list);
        getIntentData();

        //check logged status

        if(manager.isLoggedIn()){
            //add options for comments
            //show comments
            //add comment
        }


        adapter = new SearchListDisplayAdapter(this, R.layout.search_list_display_item, centersList, wm);
        lv.setAdapter(adapter);
    }

    private void getIntentData() {
        Intent intent;
        intent = getIntent();
        String jsonString = intent.getStringExtra("centers_list");
        Log.d("TAG", jsonString);

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            if (jsonObject.getInt(STATUS_TAG) == 100) {
                JSONArray centers = jsonObject.getJSONArray(CENTERS_TAG);

                for (int i = 0; i < centers.length(); i++) {
                    List<NameValuePair> currentCenterInfo = new ArrayList<NameValuePair>();
                    JSONObject currCenter = centers.getJSONObject(i);

                    currentCenterInfo.clear();

                    currentCenterInfo.add(new BasicNameValuePair(NAME_TAG,currCenter.getString(NAME_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(ID_TAG,currCenter.getString(ID_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(STREET_TAG,currCenter.getString(STREET_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(POST_TAG,currCenter.getString(POST_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(TOWN_TAG,currCenter.getString(TOWN_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(COUNTY_TAG,currCenter.getString(COUNTY_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(COUNTRY_TAG,currCenter.getString(COUNTRY_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(LAT_TAG,currCenter.getString(LAT_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(LNG_TAG,currCenter.getString(LNG_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(MAP_URL_TAG,currCenter.getString(MAP_URL_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(ADDED_BY_USER_TAG,currCenter.getString(ADDED_BY_USER_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(ACTIVITIES_TAG, currCenter.getString(ACTIVITIES_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(RATING_TAG,currCenter.getString(RATING_TAG)));
                    currentCenterInfo.add(new BasicNameValuePair(DESCRIPTION_TAG,currCenter.getString(DESCRIPTION_TAG)));

                    centersList.add(new Item(currentCenterInfo));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}