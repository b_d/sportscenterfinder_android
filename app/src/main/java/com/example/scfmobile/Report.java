package com.example.scfmobile;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

public class Report {

    List<NameValuePair> list = new ArrayList<NameValuePair>();
    public Report(List<NameValuePair> _list) {
        this.list = _list;
    }
    private String getCorrectValue(String tag){
        for(int i=0; i<list.size();i++){
            if(list.get(i).getName().equals(tag)){
                return list.get(i).getValue();
            }
        }
        return "";
    }

    public String getTime() {
        return getCorrectValue("time");
    }
    public String getErrorId() {
        return getCorrectValue("report_id");
    }
    public String getCenterName() {
        return getCorrectValue("center_name");
    }
    public String getSuggestion() {
        return getCorrectValue("suggestion");
    }
    public String getUsername() {
        return getCorrectValue("user");
    }
    public String getCenterId() {
        return getCorrectValue("id_center");
    }
    public String getProblem() {
        return getCorrectValue("problem");
    }
    public String getStatus() {
        return getCorrectValue("status");
    }
    public String getEmail() {
        return getCorrectValue("email");
    }
}
