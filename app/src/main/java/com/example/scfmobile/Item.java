package com.example.scfmobile;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

public class Item {
    List<NameValuePair> list = new ArrayList<NameValuePair>();
    String STATUS_TAG = "status";
    String CENTERS_TAG = "centers";
    String NAME_TAG = "name";
    String ID_TAG = "center_id";
    String LAT_TAG = "lat";
    String LNG_TAG = "lng";
    String STREET_TAG = "street";
    String TOWN_TAG = "town";
    String COUNTY_TAG = "county";
    String COUNTRY_TAG = "country";
    String ACTIVITIES_TAG = "activities";
    String ACTIVITY_ID_TAG = "activity_id";
    String ACTIVITY_TAG = "activity";
    String RATING_TAG = "rating";
    String POST_TAG = "post";
    String DESCRIPTION_TAG = "description";
    String ADDED_BY_USER_TAG = "added_by_user";
    String MAP_URL_TAG = "url";

    public Item(List<NameValuePair> _list) {
        this.list = _list;
    }

    private String getCorrectValue(String tag){
        for(int i=0; i<list.size();i++){
            if(list.get(i).getName().equals(tag)){
                return list.get(i).getValue();
            }
        }
        return "";
    }

    public String getName() {
        return getCorrectValue(NAME_TAG);
    }
    public String getId() {
        return getCorrectValue(ID_TAG);
    }

    public String getStreet() {
        return getCorrectValue(STREET_TAG);
    }


    public String getPost() {
        return getCorrectValue(POST_TAG);
    }


    public String getTown() {
        return getCorrectValue(TOWN_TAG);
    }


    public String getCounty() {
        return getCorrectValue(COUNTY_TAG);
    }


    public String getCountry() {
        return getCorrectValue(COUNTRY_TAG);
    }


    public String getDescription() {
        return getCorrectValue(DESCRIPTION_TAG);
    }

    public String getRating() {
        return getCorrectValue(RATING_TAG);
    }

    public String getUrl() {
        return getCorrectValue(MAP_URL_TAG);
    }

    public String getComments() {
        return "";
    }


}
